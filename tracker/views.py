from django.shortcuts import redirect
from django.urls import reverse


def redirection(request):
    return redirect(reverse("list_projects"))
