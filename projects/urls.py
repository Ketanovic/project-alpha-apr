from django.urls import path
from projects.views import (
    create_project,
    list_projects,
    show_project,
)

urlpatterns = [
    path("", list_projects, name="home"),
    path("projects/", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("projects/create/", create_project, name="create_project"),
]
